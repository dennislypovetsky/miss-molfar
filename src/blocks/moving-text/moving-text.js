// https://youtu.be/Tae96ze3xwY

function movingText(textPathSelector, textContainerSelector) {
  let textPath = document.querySelector(textPathSelector);
  let textContainer = document.querySelector(textContainerSelector);
  let path = document.querySelector(textPath.getAttribute('href'));
  let pathLength = path.getTotalLength() / 2;

  function updateTextPathOffset(offset) {
    textPath.setAttribute('startOffset', offset);
  }

  updateTextPathOffset(pathLength);

  function onScroll() {
    requestAnimationFrame(function () {
      let rect = textContainer.getBoundingClientRect();
      let scrollPercent = rect.y / window.innerHeight;

      updateTextPathOffset(scrollPercent * 0.95 * pathLength);
    });
  }

  window.addEventListener('scroll', onScroll);
}

movingText('.moving-text__textpath--org', '.moving-text--org');
movingText('.moving-text__textpath--for-whom', '.moving-text--for-whom');
