(function () {
  file.onchange = function (event) {
    const filesCount = event.target.files.length;
    let newLabelText = `Добавлено ${filesCount} фото`;
    if (filesCount === 0) {
      newLabelText = 'Добавьте 2 фото — в полный рост и портрет';
    }
    const labels = document.getElementsByClassName("form__label--file");
    for(let label of labels) {
      label.innerText = newLabelText;
    }
  };
})();
