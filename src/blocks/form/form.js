(function () {
  regForm.onsubmit = async (e) => {
    e.preventDefault();
    let response = await fetch('/registration.php', {
      method: 'POST',
      body: new FormData(regForm)
    });
    if (response.ok) {
      await response.json();
      onDone(regForm, {
        'alert-type': 'success'
      });
    }
  };

  function onDone(form, result) {
    if (result['alert-type'] && result['alert-type'] === 'success') {
      form.classList.add('form--success');
      setTimeout(function(){
        form.classList.remove('form--success');
      },3000);
      setTimeout(function(){
        form.reset();
        var event = new Event('change');
        file.dispatchEvent(event);
      },1000);
    }
  }
})();
