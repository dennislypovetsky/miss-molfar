(function () {
  regForm.onsubmit = async (e) => {
    e.preventDefault();
    let response = await fetch('/registration.php', {
      method: 'POST',
      body: new FormData(regForm)
    });
    if (response.ok) {
      await response.json();
      onDone(regForm, {
        'alert-type': 'success'
      });
    }
  };

  function onDone(form, result) {
    if (result['alert-type'] && result['alert-type'] === 'success') {
      form.classList.add('form--success');
      setTimeout(function(){
        form.classList.remove('form--success');
      },3000);
      setTimeout(function(){
        form.reset();
        var event = new Event('change');
        file.dispatchEvent(event);
      },1000);
    }
  }
})();

(function () {
  file.onchange = function (event) {
    const filesCount = event.target.files.length;
    let newLabelText = `Добавлено ${filesCount} фото`;
    if (filesCount === 0) {
      newLabelText = 'Добавьте 2 фото — в полный рост и портрет';
    }
    const labels = document.getElementsByClassName("form__label--file");
    for(let label of labels) {
      label.innerText = newLabelText;
    }
  };
})();

// https://youtu.be/Tae96ze3xwY

function movingText(textPathSelector, textContainerSelector) {
  let textPath = document.querySelector(textPathSelector);
  let textContainer = document.querySelector(textContainerSelector);
  let path = document.querySelector(textPath.getAttribute('href'));
  let pathLength = path.getTotalLength() / 2;

  function updateTextPathOffset(offset) {
    textPath.setAttribute('startOffset', offset);
  }

  updateTextPathOffset(pathLength);

  function onScroll() {
    requestAnimationFrame(function () {
      let rect = textContainer.getBoundingClientRect();
      let scrollPercent = rect.y / window.innerHeight;

      updateTextPathOffset(scrollPercent * 0.95 * pathLength);
    });
  }

  window.addEventListener('scroll', onScroll);
}

movingText('.moving-text__textpath--org', '.moving-text--org');
movingText('.moving-text__textpath--for-whom', '.moving-text--for-whom');

